package com.techuniversity.srvprod.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")
public class ServiciosControlller {

   @Autowired
    ServiciosService serviciosService;

   @GetMapping("/servicios")
   public List getServicios(){
       return serviciosService.getAll();
   }
   @PostMapping("/addServicio")
   public String addServicio(@RequestBody String newServicio){

       try{
           serviciosService.insert(newServicio);
           return "ok";
       }catch (Exception ex){
           return ex.getMessage();
       }
   }
    //Se manda en el cuerpo de la solicitud solo de forma didactica pero no se debe implementar asi
    //en un ambiente productivo
    @PostMapping("/addServicios")
    public String addServicios(@RequestBody String newServicios) {

        try {
            serviciosService.inserBatch(newServicios);
            return "ok";
        } catch (Exception ex) {
            return ex.getMessage();
        }

    }

    @GetMapping("/serviciosFiltro")
    public List getServiciosFilter(@RequestBody String filtro){
        return serviciosService.getFiltrados(filtro);
    }


    @PutMapping("/updateServicioFiltro")
    public String updateServicioFiltro(@RequestBody String data){
       try{
           JSONObject obj = new JSONObject(data);
           String filtro = obj.getString("filtro");
           String updates = obj.getString("servicios");
           serviciosService.update(filtro,updates);
           return "ok";
       } catch (Exception ex) {
          return ex.getMessage();
       }
    }
}
